# dalci_repo_3party

dalci_repo_3party is a repository of Arch Linux packages primarily for myself.

**Warning**: This repo is not meant for public use yet. The only reason this repo is public is because it has to be public for me to test it out.


### How to use it?

- First, install the primary key - it can then be used to install the keyring:

  ~~~sh
  pacman-key --recv-key 5874D2437CD5BBB3 --keyserver keyserver.ubuntu.com
  pacman-key --lsign-key 5874D2437CD5BBB3
  ~~~

- Append (adding to the **end** of the file) to `/etc/pacman.conf`:
 
  ~~~ini
  [dalci_repo_3party]
  SigLevel = Required DatabaseOptional
  Server = https://gitlab.com/dalci/$repo/-/raw/main/$arch
  ~~~

- Sync repositories and update your system:

  ~~~sh
  sudo pacman -Syyu
  ~~~

- Install the Keyring

  ~~~sh
  sudo pacman -S dalci-keyring
  ~~~
