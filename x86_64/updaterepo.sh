#!/bin/bash

rm -f dalci_repo_3party*

echo "########"
echo "repo-add"
echo "########"


repo-add -s -n -R dalci_repo_3party.db.tar.gz *.pkg.tar.zst

sleep 1

# Removing the symlinks because GitLab can't handle them.
rm dalci_repo_3party.db
rm dalci_repo_3party.db.sig
rm dalci_repo_3party.files
rm dalci_repo_3party.files.sig

# Renaming the tar.gz files without the extension.
mv dalci_repo_3party.db.tar.gz dalci_repo_3party.db
mv dalci_repo_3party.db.tar.gz.sig dalci_repo_3party.sig
mv dalci_repo_3party.files.tar.gz dalci_repo_3party.files
mv dalci_repo_3party.files.tar.gz.sig dalci_repo_3party.files.sig

echo -e "\n\e[1;32m==> Packages in the repo have been updated\e[0m"
